(function ($, window, undefined) {
  const errorMessages = {
    valueMissing: {
      default: 'Значение не должно быть пустым',
      email: 'Введите почту',
      phone: 'Введите номер телефона',
      terms: 'Вы должны согласиться с условями',
      size: 'Выберите размер',
      promocode: 'Введите промокод',
      name: 'Введите имя',
      firstname: 'Введите имя',
      lastname: 'Введите фамилию',
      password: 'Введите пароль',
      comment: 'Оставьте комментарий',
      rating: 'Поставьте оценку',
      number: 'Укажите число',
    },
    typeMismatch: {
      default: 'Неверный формат',
      phone: 'Неверный формат телефона',
      email: 'Неверный формат почты',
    },
    tooShort: {
      default: 'Недостаточно символов',
      password: 'Не менее 6 символов',
      password_new: 'Не менее 6 символов',
      password_old: 'Не менее 6 символов',
    },
    tooLong: {
      default: 'Слишком длинное значение',
    },
    rangeOverflow: {
      default: 'Значение больше максимального',
    },
    rangeUnderflow: {
      default: 'Значение меньше минимального',
    },
  }

  const processRequestError = function ($form, errors) {
    Object.keys(errors).map(field => {
      const formTag = $form[0].tagName
      let message = errors[field]
      let $fields = formTag == 'INPUT' || formTag == 'TEXTAREA' ? $form : $form.find(`[name="${field}"], [name^="${field}["]`)

      if ($form.attr('id')) {
        $fields = $fields.add(`[name="${field}"][form="${$form.attr('id')}"], [name^="${field}["][form="${$form.attr('id')}"]`)
      }

      $fields.each((i, input) => {
        const isToggle = input.type == 'checkbox' || input.type == 'radio'
        const $input = $(input)
        const $field = isToggle ? $input.closest('.toggle') : $input.closest('.field')
        const group = $input.data('fieldset')

        if (isToggle) {
          $input.data({ valid: false }).trigger('invalid')
        }

        if (group) {
          const $group = $input.parents(`.fieldset[data-fieldset="${group}"]`)
          let $error = $group.find('.fieldset__error')
          message = errorMessages.valueMissing[group] || message

          if (!$error.length) {
            $error = $(`<div class="fieldset__error"></div>`).appendTo($group)
            $error[0].offsetWidth
          }
          $error.html(message)
          $group.addClass('is-error')
        }
        else if (!isToggle) {
          let $error = $field.find('.field__error')
          if (!$error.length) {
            $error = $('<span class="field__error" />')
            $field.append($error)
            $error[0].offsetWidth
          }
          $error.html(message)
        }

        $field.removeClass('is-ok').addClass('is-error')
      })

      if (formTag == 'FORM') {
        const $submit = $form.find('[type="submit"]')
        $form
          .off('.catch-change')
          .on('valid.catch-change invalid.catch-change', (e) => {
            $submit.removeClass('is-error')
          })
        $submit.addClass('is-error')
      }
    })
  }

  const testInput = function (input) {
    const error = {}

    // console.log(input.name, input.disabled, input.value, input.dataset && input.dataset.isInputmask == "true" ? 'inputmask' : '', input.validity.valid);
    if (input.disabled) {
      // Что-то делать или нет?
      console.log(input.name, 'disabled')
    }
    else if (input.validity.valueMissing) {
      error[input.name] = errorMessages.valueMissing[input.name || input.type] || errorMessages.valueMissing['default']
      // Поле не заполнено
    }
    else if (input.value.length && (input.type == 'email' || input.name == 'email') && !Inputmask.isValid(input.value.toLowerCase(), { alias: 'email' })) {
      // Слишком не email
      error[input.name] = errorMessages.typeMismatch['email'] || errorMessages.typeMismatch['default']
    }
    else if (input.dataset && input.dataset.isInputmask == 'true' && !$(input).inputmask('isComplete')) {
      error[input.name] = errorMessages.typeMismatch[input.name] || errorMessages.typeMismatch['default']
    }
    else if (input.validity.tooShort || input.minlength && input.value.length < input.minlength) {
      // Слишком короткий
      error[input.name] = errorMessages.tooShort[input.name] || errorMessages.tooShort['default']
    }
    else if (input.validity.tooLong) {
      // Слишком длинный
      error[input.name] = errorMessages.tooLong[input.name] || errorMessages.tooLong['default']
    }
    else if (input.validity.typeMismatch) {
      // Работает только на email и url, лучше использовать для этого inputmask
      error[input.name] = errorMessages.typeMismatch[input.name] || errorMessages.typeMismatch[input.type] || errorMessages.typeMismatch['default']
    }
    else if (input.validity.rangeOverflow) {
      // Число больше максимального
      error[input.name] = errorMessages.rangeOverflow[input.name] || errorMessages.rangeOverflow['default']
    }
    else if (input.validity.rangeUnderflow) {
      // Число меньше минимального
      error[input.name] = errorMessages.rangeUnderflow[input.name] || errorMessages.rangeUnderflow['default']
    }

    return error
  }

  $.fn.formClearError = function () {
    this.find('.field.is-error, .toggle.is-error, .fieldset.is-error').removeClass('is-error')
    // this.find(':checkbox, :radio').each((inputIndex, index) => {
    //     input.setCustomValidity('');
    //     input.reportValidity();
    // });
  }

  $.fn.formError = function (errors = {}) {
    processRequestError(this, errors)
  }

  $.fn.inputValidate = function () {
    const errors = testInput(this[0])
    processRequestError(this, errors)
    return errors
  }

  $.fn.formValidate = function () {
    this.formClearError()
    const $inputs = this.find('input, textarea')
    let errors = {}

    $inputs.each((inputIndex, input) => {
      errors = { ...errors, ...testInput($inputs.eq(inputIndex)[0]) }
    })

    // processRequestError(this, errors);
    processRequestError(this, errors)
    return errors
  }

  $.fn.form = function ({ success: successCallback, error: errorCallback, beforeSend: beforeSendCallback, complete: completeCallback } = {}) {
    this.each(function (i, form) {
      const $form = $(form)
      let lock

      $form.off('.form').on('submit.form', function (e) {
        e.preventDefault()

        const validateErrors = $form.formValidate()

        if (lock || !$.isEmptyObject(validateErrors)) {
          return
        }

        lock = true
        let data
        const $files = $form.find('[type="file"]')

        if ($files.length) {
          data = new FormData(form)
          $files.each(function (i, input) {
            data.append(input.name, input)
          })
        } else {
          data = $form.serialize()
        }

        const $submit = $form.find('button[type=submit]').prop('disabled', true)

        $form.addClass('is-loading').formClearError()

        $.ajax({
          url: $form.attr('action'),
          type: $form.attr('method') || 'post',
          dataType: 'json',
          data: data,
          contentType: (data instanceof FormData) ? false : 'application/x-www-form-urlencoded',
          processData: !(data instanceof FormData),
          beforeSend: function (xhr, settings) {
            if (beforeSendCallback) {
              beforeSendCallback.call(null, xhr, form, settings)
            }
          },
          success: function (response) {
            if (successCallback) {
              successCallback.call(null, response, form)
            }
          },
          error: function (xhr) {
            app
              .processError(xhr, {
                request: errors => processRequestError($form, errors)
              })
              .then(errorCallback || function () {})
          },
          complete: function (response) {
            lock = false
            $form.removeClass('is-loading')
            $submit.prop('disabled', false)

            if (completeCallback) {
              completeCallback.call(null, response, form)
            }
          }
        })
      })
    })
  }

})(jQuery, window);

