app.bind('default', function (context) {
  const $textToggles = $('[js-toggle-text]', context)
  $textToggles.each(function () {
    const $textToggle = $(this)
    const $textToggleAction = $textToggle.find('[js-toggle-text-action]')
    $textToggleAction.on('click', function () {
      $textToggle.toggleClass('is-open')
    })
  })
})