app.bind('default formus', function (context) {
  $('[js-formus]', context).each(function () {
    const $form = $(this)
    const method = $form.attr('method')
    const url = $form.attr('action')
    const reload = $form.data('reload') ? $form.data('reload') : false
    let request
    $form.off('.formus').on('submit.formus', function (e) {
      e.preventDefault()
      $form.addClass('is-load')
      request && request.abort()

      request = $.ajax({
        url: url,
        type: method,
        dataType: 'html',
        data: $form.serialize(),
        success (data) {
        },
        complete () {
          if (!reload) {
            $form.removeClass('is-load')
            $form.addClass('is-complete')
            request = null
          } else {
            document.location.reload(true)
          }
        }
      })
    })
  })
})
