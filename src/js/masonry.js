app.bind('default', function (context) {

  const $window = $(window)

  let $grid

  function initMasonry () {
    $grid = $('[js-masonry]').masonry({
      itemSelector: '[js-masonry-item]',
      columnWidth: '[js-masonry-item]'
    })

    $grid.imagesLoaded().progress(function () {
      $grid.masonry('layout')
    })
  }

  $window.on('load', function () {
    initMasonry()
  })

})