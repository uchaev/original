app.bind('default', function (context) {
  let $tabs = $('[js-tabs]', context)
  $tabs.each(function () {
    let $tab = $(this)
    let $tabAction = $tab.find('[js-tab-action]')
    let $tabPage = $tab.find('[js-tab-page]')

    $tabAction.on('click', function () {
      $tabAction.removeClass('is-active')
      $(this).addClass('is-active')
      let i = $(this).data('target')
      $tabPage.removeClass('is-active').filter('[data-target=' + i + ']').addClass('is-active')
    })
  })
})