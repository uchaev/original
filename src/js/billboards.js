app.bind('default', function (context) {
  const $billboards = $('[js-billboards]', context)
  $billboards.each(function () {
    const $billboard = $(this)
    const $billboardSlider = $billboard.find('[js-billboards-slider]')
    const $billboardSliderList = $billboardSlider.find('[js-billboards-slider-list]')
    const $billboardSliderNav = $billboardSlider.find('[js-billboards-slider-nav]')
    const $billboardSliderDots = $billboardSlider.find('[js-billboards-slider-dots]')

    $billboardSliderList.addClass('owl-carousel').owlCarousel({
      loop: false,
      nav: true,
      dots: true,
      margin: 0,
      navText: [],
      dotsContainer: $billboardSliderDots,
      navContainer: $billboardSliderNav,
      items: 1,
      onInitialized () {
        $billboard.addClass('is-init')
      },
      onTranslate (item) {
        let $this = $(item.currentTarget)
        $this.addClass('is-lock')
      },
      onTranslated (item) {
        const $this = $(item.currentTarget)
        $this.removeClass('is-lock')
      }
    })
  })
})

app.bind('default', function (context) {
  const $stills = $('[js-still]', context)
  $stills.each(function () {
    const $still = $(this)
    const $stillSlider = $still.find('[js-still-slider]')
    const $stillSliderList = $stillSlider.find('[js-still-slider-list]')
    const $stillSliderNav = $stillSlider.find('[js-still-slider-nav]')
    const $stillSliderDots = $stillSlider.find('[js-still-slider-dots]')

    $stillSliderList.addClass('owl-carousel').owlCarousel({
      loop: false,
      nav: true,
      dots: true,
      margin: 10,
      navText: [],
      dotsContainer: $stillSliderDots,
      navContainer: $stillSliderNav,
      items: 5,
      responsive: {
        0: {
          items: 1
        },
        500: {
          items: 2
        },
        600: {
          items: 3
        },
        1200: {
          items: 4
        },
        1300: {
          items: 5
        }
      },
      onInitialized () {
        $still.addClass('is-init')
      },
      onTranslate (item) {
        let $this = $(item.currentTarget)
        $this.addClass('is-lock')
      },
      onTranslated (item) {
        const $this = $(item.currentTarget)
        $this.removeClass('is-lock')
      }
    })
  })

})