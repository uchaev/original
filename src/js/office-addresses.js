app.bind('default', function (context) {
  const $officeAddresses = $('[js-toggle-box]', context)
  $officeAddresses.each(function () {
    const $officeAddress = $(this)
    $officeAddress.find('a').on('click', function (e) {
      e.preventDefault()
    })
    const $officeAddressClose = $officeAddress.find('[js-toggle-box-close]')
    let timeout
    $officeAddress.on('mouseenter', function () {
      clearTimeout(timeout)
      $officeAddress.addClass('is-open')
    })
    $officeAddress.on('click', function () {
      clearTimeout(timeout)
      $officeAddress.addClass('is-open')
    })
    $officeAddress.on('mouseleave', function () {
      timeout = setTimeout(() => {
        $officeAddress.removeClass('is-open')
      }, 1500)
    })
    $officeAddress.on('click', function (e) {
      e.stopPropagation()
    })
    $officeAddressClose.on('click', function (e) {
      e.stopPropagation()
      $officeAddress.removeClass('is-open')
    })
    $('body').on('click', function () {
      $officeAddress.removeClass('is-open')
    })
  })
})