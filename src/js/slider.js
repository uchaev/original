app.bind('default slider', function(context){

  const $sliders = $('[js-gallery]', context);
  $sliders.each((index, item) => {
    const $slider = $(item);

    const $sliderMain = $(item).find('[js-gallery-main]');
    const $sliderMainList = $slider.find('[js-gallery-main-list]');
    const $sliderMainNav = $slider.find('[js-gallery-main-nav]');

    $sliderMainList.addClass('owl-carousel').owlCarousel({
      loop: false,
      nav: true,
      dots: false,
      margin: 15,
      navContainer: $sliderMainNav,
      navText: [],
      items: 1,
      onInitialized() {
        $sliderMain.addClass('is-init');
      },
      onTranslate(item) {
        let $this = $(item.currentTarget);
        $this.addClass('is-lock');
      },
      onTranslated(item) {
        const $this = $(item.currentTarget);
        $this.removeClass('is-lock');
        let pos = $this.find('.owl-item.active [data-index]').data('index');
        $sliderPreviewList.find('[data-index]').removeClass('is-active').filter('[data-index='+ pos +']').addClass('is-active');
        $sliderPreviewList.trigger('to.owl.carousel', pos - 1);
      },

    });

    const $sliderPreview = $(item).find('[js-gallery-preview]');
    const $sliderPreviewList = $slider.find('[js-gallery-preview-list]');
    const $sliderPreviewItem = $slider.find('[js-gallery-preview-item]');

    $sliderPreviewList.addClass('owl-carousel').owlCarousel({
      loop: false,
      nav: false,
      dots: false,
      margin: 20,
      navText: [],
      items: 3,
      responsive:{
        0:{
          items:2,
          nav:false
        },
        1200:{
          items:3
        }
      },
      onInitialized() {
        $sliderPreview.addClass('is-init');
      },

    });

    $sliderPreviewItem.off('.slider').on('click.slider',function () {
      let pos = $(this).data('index');
      $sliderPreviewList.find('[data-index]').removeClass('is-active').filter('[data-index='+ pos +']').addClass('is-active');
      $sliderMainList.trigger('to.owl.carousel', pos - 1);
    })


  });
});
