app.bind('default', function (context) {
  const $map = $('[js-map]', context)
  if ($map.length) {
    ymaps.ready(init)
  }

  console.log($map.data('center'))
  console.log($map.data('zoom'))

  function init () {
    const myMap = new ymaps.Map($map[0], {
      // Координаты центра карты.
      // Порядок по умолчанию: «широта, долгота».
      // Чтобы не определять координаты центра карты вручную,
      // воспользуйтесь инструментом Определение координат.
      center: $map.data('center'),
      // Уровень масштабирования. Допустимые значения:
      // от 0 (весь мир) до 19.
      zoom: $map.data('zoom'),
      controls: []
    })

    let coords = $map.data('coords')
    myMap.behaviors.disable('scrollZoom')

    let geoObjects = []

    coords.split(';').forEach(function (item) {
      let a = item.replace('[', '')
      let b = a.replace(']', '')
      let c = []
      c.push(parseFloat(b.split(',')[0]))
      c.push(parseFloat(b.split(',')[1]))
      let myPlacemark = new ymaps.Placemark(c, {
        hintContent: '',
        balloonContent: ''
      })

      myMap.geoObjects.add(myPlacemark)

    })
  }
})