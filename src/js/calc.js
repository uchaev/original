app.bind('default', function (context) {
  $('[js-calc-panel]', context).stick_in_parent({ offset_top: 60 })
  $('[js-calc-mobile]', context).on('click', function () {
    $.scrollTo($('[js-calc-side]'), 200, { offset: { top: -60 } })
  })
  $('[js-calc]', context).each(function () {
    const $calcForm = $(this)

    let $inputHeight = $calcForm.find('[js-height-window]')
    let $inputHeightA = $calcForm.find('[js-height-a-window]')
    let $inputWidth = $calcForm.find('[js-width-window]')
    let $fieldsErrorSize = $calcForm.find('[js-fields-size]')

    const $calcPriceProduct = $calcForm.find('[js-result-product]')
    const $calcPriceProductTotal = $calcForm.find('[js-result-product-total]')
    const $calcPriceMontag = $calcForm.find('[js-result-montag]')
    const $calcPriceOtdelka = $calcForm.find('[js-result-otdelka]')
    const $calcTotal = $calcForm.find('[js-calc-total]')
    const $calculateButton = $calcForm.find('[js-calc-button]')
    const $ButtonAddOrder = $calcForm.find('[js-calc-button-add-order]')

    $inputHeight.add($inputWidth).on('input', function () {
      $fieldsErrorSize.removeClass('is-visible')
    })
    $('[js-windows-item]').on('click', function () {
      $calcTotal.addClass('is-hidden')
    })

    $calculateButton.on('click', function () {
      $fieldsErrorSize.removeClass('is-visible')
      console.log()

      console.log(parseInt($('[js-window-input]').val()))
      let errors = []
      if (parseInt($('[js-window-input]').val()) === 45) {
        if ($inputHeightA.val() === '' || $inputHeightA.val() === 0) {
          errors.push('heightA')
        }
      }
      if ($inputHeight.val() === '' || $inputHeight.val() > 2000 || $inputHeight.val() < 700) {
        errors.push('height')
      }
      if ($inputWidth.val() === '' || $inputWidth.val() > 2000 || $inputWidth.val() < 700) {
        errors.push('width')
      }
      if (errors.length === 0) {
        $calcTotal.removeClass('is-hidden')
        $.scrollTo($calcTotal, 150, { offset: { top: -60 } })
      } else {
        $.scrollTo($inputHeight, 100, { offset: { top: -120 } })
        $fieldsErrorSize.addClass('is-visible')
      }
    })
    $ButtonAddOrder.on('click', function (e) {
      e.preventDefault()
      e.stopPropagation()
      $calcTotal.addClass('is-hidden')
      $.notify('Заказ добавлен в корзину', { globalPosition: 'bottom left', className: 'info', gap: 4 })
      $.scrollTo($('[js-calc-side]'), 200, { offset: { top: -60 } })

      let total = {}

      $calcForm.find('input').each(function () {
        const $input = $(this)
        if ($input.attr('name') && $input.attr('name').length > 0) {
          total[$input.attr('name')] = $input.val()
        }
      })
      $calcForm.find('select').each(function () {
        const $select = $(this)
        const $options = $select.find('option')
        const $seelectOption = $options.filter('[value= ' + $select.val() + ']').text().trim()
        if ($select.attr('name') && $select.attr('name').length > 0) {
          total[$select.attr('name')] = $seelectOption
        }
      })

      const $windowTypeVal = $('[js-window-input]').val()
      console.log(parseInt($windowTypeVal))
      switch (parseInt($windowTypeVal)) {
        case 1:
          total.tip = 'Одностворчатое глухое окно'
          break
        case 11:
          total.tip = 'Одностворчатое окно с поворотно-откидной створкой'
          break
        case 2:
          total.tip = 'Двустворчатое глухое окно'
          break
        case 21:
          total.tip = 'Двустворчатое окно с поворотно-откидной и глухой створками'
          break
        case 22:
          total.tip = 'Двустворчатое окно с поворотно-откидной и поворотной створками'
          break
        case 3:
          total.tip = 'Трехстворчатое глухое окно'
          break
        case 31:
          total.tip = 'Трехстворчатое окно с поворотно-откидной и двумя глухими створками'
          break
        case 32:
          total.tip = 'Трехстворчатое окно с поворотно-откидной, глухой и поворотной створками'
          break
        case 45:
          total.tip = 'Балконный блок'
          break
        default:
      }

      total['product_price'] = $calcPriceProduct.text().trim()
      total['product_price_total'] = $calcPriceProductTotal.text().trim()
      total['install_price'] = $calcPriceMontag.text().trim()
      total['install_trim'] = $calcPriceOtdelka.text().trim()

      function generateID (min, max) {

        let id = getRandomArbitrary(min, max)
        let array = JSON.parse(localStorage.getItem('orders'))
        if (array) {
          if (array.length > 0) {
            if (array.find((item) => item.id === id)) {
              generateID(min, max)
            }
            return id
          }
          return id
        }
        return id
      }

      total['id'] = generateID(0, 9999)
      console.log(total)

      if (localStorage.getItem('orders')) {
        if (JSON.parse(localStorage.getItem('orders')).length > 0) {
          let array = JSON.parse(localStorage.getItem('orders'))
          array.push(total)
          localStorage.setItem('orders', JSON.stringify(array))
          updateCart()
        }
        else {
          let array = []
          array.push(total)
          localStorage.setItem('orders', JSON.stringify(array))
          updateCart()
        }
      }
      else {
        let array = []
        array.push(total)
        localStorage.setItem('orders', JSON.stringify(array))
        updateCart()
      }
    })
  })
  updateCart()

  function updateCart () {
    let $card = $('[js-calc-panel]')
    let $cardOrders = $('[js-calc-orders]')
    let $cardOrdersTotal = $('[js-calc-orders-total]')
    if (localStorage.getItem('orders')) {
      let array = JSON.parse(localStorage.getItem('orders'))
      if (array.length > 0) {
        $card.addClass('is-filled')
        $cardOrders.html('')
        let totalPrice = 0
        array.forEach((item, index) => {
          totalPrice += parseInt(item.product_price_total)
          let $order = $(`<div class="calc-info__orders-item"> <div class="calc-info__order"> <div class="calc-info__order-wrapper"> <div class="calc-info__order-main"> <div class="calc-info__order-title"> ${index + 1}. ${item.profile} </div> <div class="calc-info__order-desc"> ${item.width} × ${item.height} мм ${item.install === 'Требуется' ? 'монтаж' : ''} ${item.decking !== 'Не требуется' ? ',отделка' : ''} </div> <div class="calc-info__order-price"> ${item.product_price_total} ₽ </div> </div> <div class="calc-info__order-side"> <div js-order-remove class="calc-info__order-close" data-id=${item.id}></div> </div> </div> </div> </div>`)
          $order.find('[js-order-remove]').on('click', function () {
            array = array.filter((el) => el.id !== $(this).data('id'))
            localStorage.setItem('orders', JSON.stringify(array))
            updateCart()
          })
          $cardOrders.append($order)
        })
        $cardOrdersTotal.html(totalPrice)
      } else {
        $card.removeClass('is-filled')
      }
    } else {
      $card.removeClass('is-filled')
    }
  }

  function getRandomArbitrary (min, max) {
    return Math.round(Math.random() * (max - min) + min)
  }

  $('[js-calc-form]', context).on('submit', function (e) {
    e.preventDefault()
    const $form = $(this)
    const mod = $form.data('mod')
    const URL = $form.attr('action')
    const type = $form.attr('type')
    const $phone = $form.find('[js-phone]')

    let data = {}
    data.phone = $phone.val()
    if (mod === 'card') {
      data.items = localStorage.getItem('orders')
      data.total = $('[js-calc-orders-total]').text()
    }

    $form.addClass('is-load')
    $.ajax({
      url: URL,
      type: type,
      data,
      complete () {
        $form.removeClass('is-load')
      },
      success () {
        $form.addClass('is-success')
        if (mod === 'card') {
          let array = []
          localStorage.setItem('orders', JSON.stringify(array))
        }
      }
    })
  })
})