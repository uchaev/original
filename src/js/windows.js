app.bind('default', function (context) {
  const $windows = $('[js-windows-item]', context)
  const $input = $('[js-window-input]')

  $('[js-windows]').each(function () {
    const $windowItems = $(this).find('[js-windows-item]')
    $windowItems.each(function () {
      const $windowItem = $(this)
      const $link = $windowItem.find('.windows-item__link')
      const $windowItemImgs = $windowItem.find('[js-img]')

      $link.on('click', function (e) {
        e.stopPropagation()
        $windowItems.not($windowItem).removeClass('is-open')
        $windowItem.toggleClass('is-open')
      })
      $windowItemImgs.each(function () {
        const $windowItemImg = $(this)
        $windowItemImg.on('click', function () {
          const $imgBox = $('[js-windows-img-main-box]')
          const $img = $(this)
          $input.val($img.data('id'))
          $input.change()
          console.log($input.val())
          $windowItems.removeClass('is-open')
          $('[js-windows-img-additional-box]').removeClass('is-active')
          $imgBox.removeClass('is-active')
          if ($img.closest('[js-windows-img-main-box]').length) {
            $('[js-img]').filter('[data-id=' + $img.data('id') + ']').closest('[js-windows-img-additional-box]').addClass('is-active')
            $img.closest($imgBox).addClass('is-active')
          }
          else {
            const $imgClone = $(this).clone(true)
            $img.closest('[js-windows-img-additional-box]').addClass('is-active')
            $img.closest($windowItem).find($imgBox).addClass('is-active').html($imgClone)
          }
        })
      })

      $windowItem.on('click', function (e) {
        e.stopPropagation()
      })

      $windowItem.on('mouseenter', function () {
        if ($(window).width() < 1000) {
          return false
        }
        $windowItem.removeClass('is-open')
        $(this).addClass('is-open')
      })
      $windowItem.on('mouseleave', function () {
        if ($(window).width() < 1000) {
          return false
        }
        $windowItem.removeClass('is-open')
      })

      $('body').on('click', function () {
        $windowItems.removeClass('is-open')
      })
    })
  })
})